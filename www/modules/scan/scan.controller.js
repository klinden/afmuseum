'use strict';

angular.module('afmuseum')
    .controller('ScanCtrl', ['$scope', '$cordovaBarcodeScanner', '$state', function($scope, $cordovaBarcodeScanner, $state) {
        $scope.scanBarcode = function() {
            $cordovaBarcodeScanner.scan().then(function(scanData) {
                $state.go('app.exhibit', {exhibitId: scanData.text});
                //$scope.barcodeData = scanData.text;
            });
        };
    }]);
